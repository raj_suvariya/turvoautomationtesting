package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.Utilities.Constants;
import com.shubham.turvoautomationtesting.Utilities.Utils;
import com.shubham.turvoautomationtesting.location.LocationListScreen;

import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.List;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

/**
 * Created by @iamBedant on 10/02/17.
 */

public class LocationTest {

    DesiredCapabilities dc;
    AndroidDriver mAndroidDriver;
    HomeScreen homeScreen;
    NavPage navPage;
    LocationListScreen locationListScreen;

    @BeforeTest
    void beforeTest(){
        dc= new DesiredCapabilities();
        Utils.appInit(dc);
        try {
            mAndroidDriver= new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), dc);
            homeScreen = new HomeScreen(mAndroidDriver);
            navPage = new NavPage(mAndroidDriver);
            locationListScreen = new LocationListScreen(mAndroidDriver);
            Thread.sleep(5000);
//            shipmentListScreen = new ShipmentListScreen(mAndroidDriver);
        }catch(Exception e){

        }
    }

    /**
     * Test - 1
     * Opens location page
     */
    @Test(priority = 1)
    public void openLocationPage() throws InterruptedException {
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.HOME);
        homeScreen.clickIconLogo();
        navPage.onLocationClick();
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.LOCATION);
        Thread.sleep(3000);
    }

    /**
     * Test - 2
     * Toggle's view between grid and list view and opens one item from each group
     */
    @Test(priority = 2)
    public void changeView() throws InterruptedException{

        for(int i=0;i<4;i++) {
            Dimension size = mAndroidDriver.manage().window().getSize();
            int y_start = (int) (size.height * 0.6);
            int y_end = (int) (size.height * 0.3);
            int x = size.width / 2;
            mAndroidDriver.swipe(x, y_start, x, y_end, 1000);
            Thread.sleep(500);
        }
        locationListScreen.locationItemClick(0);
        mAndroidDriver.navigate().back();

        locationListScreen.toggleGridListView();
        for(int i=0;i<4;i++) {
            Dimension size = mAndroidDriver.manage().window().getSize();
            int y_start = (int) (size.height * 0.6);
            int y_end = (int) (size.height * 0.3);
            int x = size.width / 2;
            mAndroidDriver.swipe(x, y_start, x, y_end, 1000);
            Thread.sleep(500);
        }
        locationListScreen.locationItemClick(0);
        mAndroidDriver.navigate().back();
        Thread.sleep(1000);
    }

    /**
     * Test - 3
     * Changes filters for locations
     */
    @Test (priority = 3)
    public void changeFilters() throws InterruptedException{

        locationListScreen.onFilterButtonClick();
        Thread.sleep(500);

        locationListScreen.onCancelButtonClick();
        Thread.sleep(500);

        locationListScreen.onFilterButtonClick();
        Thread.sleep(500);

        locationListScreen.onFilterTextClick();
        Thread.sleep(1000);

        locationListScreen.setKeyFilterEditText(0, "R");
        Thread.sleep(1000);
        mAndroidDriver.navigate().back();
        Thread.sleep(1000);
        mAndroidDriver.navigate().back();
        Thread.sleep(1000);
        Assert.assertEquals(locationListScreen.getKeyFilterEditText(0), "R");

        locationListScreen.setKeyFilterEditText(1, "H");
        Thread.sleep(1000);
        mAndroidDriver.navigate().back();
        Thread.sleep(10000);
        MobileElement doneButton = (MobileElement) (new WebDriverWait(mAndroidDriver, 30)).until(ExpectedConditions.presenceOfElementLocated(new MobileBy.ByAndroidUIAutomator("UiSelector().resourceId(\""+ Constants.APP_PACKAGE_NAME+":id/toolbar_right_text\")")));
        List<WebElement> element = (new WebDriverWait(mAndroidDriver, 30)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(new MobileBy.ByAndroidUIAutomator("UiSelector().resourceId(\""+ Constants.APP_PACKAGE_NAME+":id/filter_text\")")));
        doneButton.click();
        Thread.sleep(1000);
        Assert.assertEquals(locationListScreen.getKeyFilterEditText(1), "");

        locationListScreen.onCrossDockCheckbox();
        locationListScreen.onColdStorageCheckbox();

        locationListScreen.onResetButtonClick();
        mAndroidDriver.navigate().back();
        Thread.sleep(1000);

        Assert.assertEquals(locationListScreen.getKeyFilterEditText(0), "");
        Assert.assertEquals(locationListScreen.getKeyFilterEditText(1), "");

        locationListScreen.onSortTextClick();
        Thread.sleep(1000);

        locationListScreen.onSortingOptionItemSelected(1);
        locationListScreen.onSortingOptionItemSelected(2);
        Thread.sleep(1000);

        locationListScreen.onApplyButtonClick();
        Thread.sleep(1000);

    }

    @AfterTest(alwaysRun = true)
    public void afterTest(){
        mAndroidDriver.closeApp();
        mAndroidDriver.quit();
        homeScreen = null;
        navPage = null;
    }

}
