package com.shubham.turvoautomationtesting.accountsettings;

import com.shubham.turvoautomationtesting.Utilities.Constants;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by Shubham on 08/02/17.
 */

public class AccountSettingsScreen {
    private static final String TAG = AccountSettingsScreen.class.getSimpleName();

    private AndroidDriver mAndroidDriver;

    public AccountSettingsScreen(AndroidDriver androidDriver) {
        this.mAndroidDriver = androidDriver;
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);
    }

    @AndroidFindBy(uiAutomator = "UiSelector().textContains(\"Groups\")")
    private AndroidElement groupsSettingText;

    public void onGroupSettingsClick(){
        groupsSettingText.click();
    }

    @AndroidFindBy(uiAutomator = "UiSelector().textContains(\"Theme\")")
    private AndroidElement themeSettingText;

    public void onThemeSettingClick(){
        themeSettingText.click();
    }

    @AndroidFindBy(uiAutomator = "UiSelector().textContains(\"Change password\")")
    private AndroidElement passwordSettingText;

    public void onPasswordSettingClick(){
        passwordSettingText.click();
    }

    @AndroidFindBy(uiAutomator = "UiSelector().textContains(\"Logout\")")
    private AndroidElement logoutText;

    public void onLogoutClick(){
        logoutText.click();
    }


    @AndroidFindBy(uiAutomator = "UiSelector().textContains(\"test\")")
    private AndroidElement testGroupText;

    public void onTestGroupToggle(){
        testGroupText.click();
    }

    @AndroidFindBy(uiAutomator = "UiSelector().resourceId(\""+Constants.APP_PACKAGE_NAME+":id/apply\")")
    private AndroidElement groupSettingApplyButton;

    public void onGrourpSettingsApplyButtonClick(){
        groupSettingApplyButton.click();
    }

    @FindBys({@FindBy(id = Constants.APP_PACKAGE_NAME+":id/check_label")})
    List<WebElement> checkboxes;

    public int getNumberofCheckboxes(){
        return checkboxes.size();
    }

    @AndroidFindBy(uiAutomator = "UiSelector().resourceId(\""+Constants.APP_PACKAGE_NAME+":id/switch_button\")")
    private AndroidElement themeSettingsToggleButton;

    public String onThemeChangeToggleClick(){
        themeSettingsToggleButton.click();
        return themeSettingsToggleButton.getText();
    }

    @AndroidFindBy(uiAutomator = "UiSelector().resourceId(\""+Constants.APP_PACKAGE_NAME+":id/buttonDefaultPositive\")")
    private AndroidElement confirmLogoutButton;

    public void onConfirmLogoutButton(){
        confirmLogoutButton.click();
    }

//
//    @AndroidFindBy(uiAutomator = "UiSelector().resourceId(\""+Constants.APP_PACKAGE_NAME+":id/cross_button\")")
//    private AndroidElement backButton;
//
//    public void onBackButton(){
//        confirmLogoutButton.click();
//    }

    //
}
