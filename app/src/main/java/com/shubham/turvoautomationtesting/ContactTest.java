package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.Utilities.Constants;
import com.shubham.turvoautomationtesting.Utilities.Utils;
import com.shubham.turvoautomationtesting.contact.ContactsListScreen;

import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.List;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

/**
 * Created by @iamBedant on 10/02/17.
 */

public class ContactTest {

    DesiredCapabilities dc;
    AndroidDriver mAndroidDriver;
    HomeScreen homeScreen;
    NavPage navPage;
    ContactsListScreen contactsListScreen;

    @BeforeTest
    void beforeTest(){
        dc= new DesiredCapabilities();
        Utils.appInit(dc);
        try {
            mAndroidDriver= new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), dc);
            homeScreen = new HomeScreen(mAndroidDriver);
            navPage = new NavPage(mAndroidDriver);
            contactsListScreen = new ContactsListScreen(mAndroidDriver);
            Thread.sleep(5000);
//            shipmentListScreen = new ShipmentListScreen(mAndroidDriver);
        }catch(Exception e){

        }
    }

    /**
     * Test - 1
     * Opens contacts page
     */
    @Test(priority = 1)
    public void openContactsPage() throws InterruptedException {
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.HOME);
        homeScreen.clickIconLogo();
        navPage.onContactClick();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.CONTACTS);
        Thread.sleep(3000);
    }

    /**
     * Test - 2
     * Toggle's view between grid and list view and opens one item from each group
     */
    @Test(priority = 2)
    public void changeView() throws InterruptedException{
        for(int i=0;i<4;i++) {
            Dimension size = mAndroidDriver.manage().window().getSize();
            int y_start = (int) (size.height * 0.6);
            int y_end = (int) (size.height * 0.3);
            int x = size.width / 2;
            mAndroidDriver.swipe(x, y_start, x, y_end, 1000);
            Thread.sleep(500);
        }
        contactsListScreen.contactsItemClick(0);
        mAndroidDriver.navigate().back();

        contactsListScreen.toggleGridListView();
        for(int i=0;i<4;i++) {
            Dimension size = mAndroidDriver.manage().window().getSize();
            int y_start = (int) (size.height * 0.6);
            int y_end = (int) (size.height * 0.3);
            int x = size.width / 2;
            mAndroidDriver.swipe(x, y_start, x, y_end, 1000);
            Thread.sleep(500);
        }
        contactsListScreen.contactsItemClick(0);
        mAndroidDriver.navigate().back();
        Thread.sleep(1000);
    }

    /**
     * Test - 3
     * Changes filters for contacts
     */
    @Test (priority = 3)
    public void changeFilters() throws InterruptedException{

        contactsListScreen.onFilterButtonClick();
        Thread.sleep(500);

        contactsListScreen.onCancelButtonClick();
        Thread.sleep(500);

        contactsListScreen.onFilterButtonClick();
        Thread.sleep(500);

        contactsListScreen.onFilterTextClick();
        Thread.sleep(500);

        contactsListScreen.setKeyFilterEditText(0, "R");
        Thread.sleep(1000);
        mAndroidDriver.navigate().back();
        Thread.sleep(1000);
        mAndroidDriver.navigate().back();
        Thread.sleep(1000);
        Assert.assertEquals(contactsListScreen.getKeyFilterEditText(0), "R");

        contactsListScreen.setKeyFilterEditText(1, "H");
        Thread.sleep(2000);
        mAndroidDriver.navigate().back();
        Thread.sleep(10000);
        MobileElement doneButton = (MobileElement) (new WebDriverWait(mAndroidDriver, 30)).until(ExpectedConditions.presenceOfElementLocated(new MobileBy.ByAndroidUIAutomator("UiSelector().resourceId(\""+ Constants.APP_PACKAGE_NAME+":id/toolbar_right_text\")")));
        List<WebElement> element = (new WebDriverWait(mAndroidDriver, 30)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(new MobileBy.ByAndroidUIAutomator("UiSelector().resourceId(\""+ Constants.APP_PACKAGE_NAME+":id/filter_text\")")));
        doneButton.click();

        Thread.sleep(1000);
        Assert.assertEquals(contactsListScreen.getKeyFilterEditText(1), "");

        contactsListScreen.onResetButtonClick();
        mAndroidDriver.navigate().back();
        Thread.sleep(1000);

        Assert.assertEquals(contactsListScreen.getKeyFilterEditText(0), "");
        Assert.assertEquals(contactsListScreen.getKeyFilterEditText(1), "");

        contactsListScreen.onSortTextClick();
        Thread.sleep(1000);

        contactsListScreen.onSortingOptionItemSelected(1);
        contactsListScreen.onSortingOptionItemSelected(2);
        contactsListScreen.onSortingOptionItemSelected(3);
        Thread.sleep(1000);

        contactsListScreen.onApplyButtonClick();
        Thread.sleep(1000);

    }

    @AfterTest(alwaysRun = true)
    public void afterTest(){
        mAndroidDriver.closeApp();
        mAndroidDriver.quit();
        homeScreen = null;
        navPage = null;
    }

}
