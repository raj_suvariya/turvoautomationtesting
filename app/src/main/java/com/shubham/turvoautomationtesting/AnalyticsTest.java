package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.Utilities.Utils;

import org.junit.Assert;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;

import io.appium.java_client.android.AndroidDriver;

/**
 * Created by @iamBedant on 10/02/17.
 */

public class AnalyticsTest {

    DesiredCapabilities dc;
    AndroidDriver mAndroidDriver;
    HomeScreen homeScreen;
    NavPage navPage;
    AnalyticsScreen shipmentListScreen;
    ShipmentSummeryScreen shipmentSummeryScreen;
    FilterScreen filterScreen;

    @BeforeTest
    void beforeTest() {
        dc = new DesiredCapabilities();
        Utils.appInit(dc);
        try {
            mAndroidDriver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), dc);
            homeScreen = new HomeScreen(mAndroidDriver);
            navPage = new NavPage(mAndroidDriver);
            shipmentSummeryScreen = new ShipmentSummeryScreen(mAndroidDriver);
            shipmentListScreen = new AnalyticsScreen(mAndroidDriver);
            filterScreen = new FilterScreen(mAndroidDriver);
            Thread.sleep(5000);
//            shipmentListScreen = new ShipmentListScreen(mAndroidDriver);
        } catch (Exception e) {

        }
    }

    @Test(priority = 1)
    public void testA() throws InterruptedException {
        Assert.assertEquals(homeScreen.getToolBarText(), "Home");
        homeScreen.clickIconLogo();
        navPage.onAnalyticsClick();
        Assert.assertEquals(homeScreen.getToolBarText(), "Analytics");
        Thread.sleep(5000);
        shipmentListScreen.onShipmentSummeryClick();
        Assert.assertEquals(homeScreen.getToolBarText(), "Shipment Summary");
        shipmentSummeryScreen.onShipmentSummeryClick();
        Assert.assertEquals(homeScreen.getToolBarText(), "Filters");
        Assert.assertEquals(filterScreen.getToDateText(),"Feb 15,2017");
        Assert.assertEquals(filterScreen.getFromeDateText(),"Feb 01,2017");

        //set a new Date and Test
        filterScreen.clickOnDate();
        DatePickerClass cls = new DatePickerClass(mAndroidDriver);
        cls.setDate(2016,2,25);
        Assert.assertEquals(filterScreen.getFromeDateText(),"Feb 25,2016");

        //


        filterScreen.fromClearClicked();
        filterScreen.toClearClicked();
        Assert.assertEquals(filterScreen.getFromeDateText(),"");
        Assert.assertEquals(filterScreen.getToDateText(),"");
        filterScreen.resetClicked();
        Assert.assertEquals(filterScreen.getFromeDateText(),"Feb 01,2017");
        Assert.assertEquals(filterScreen.getToDateText(),"Feb 15,2017");

        //Click Open Popup
        filterScreen.clickPopUp();
        //
        filterScreen.clickFirstPopup();
        filterScreen.clickSecondPopup();
        filterScreen.clickDone();
        //

        filterScreen.cancelClicked();
        Assert.assertEquals(homeScreen.getToolBarText(), "Shipment Summary");

    }

    @AfterTest(alwaysRun = true)
    public void afterTest() {
        mAndroidDriver.closeApp();
        mAndroidDriver.quit();
        homeScreen = null;
        navPage = null;
    }

}
