package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.Utilities.Utils;

import org.junit.Assert;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;

import io.appium.java_client.android.AndroidDriver;

/**
 * Created by @iamBedant on 10/02/17.
 */

public class ShipmentTest {

    DesiredCapabilities dc;
    AndroidDriver mAndroidDriver;
    HomeScreen homeScreen;
    NavPage navPage;
    ShipmentListScreen shipmentListScreen;

    @BeforeTest
    void beforeTest(){
        dc= new DesiredCapabilities();
        Utils.appInit(dc);
        try {
            mAndroidDriver= new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), dc);
            homeScreen = new HomeScreen(mAndroidDriver);
            navPage = new NavPage(mAndroidDriver);
            shipmentListScreen = new ShipmentListScreen(mAndroidDriver);
            Thread.sleep(5000);
//            shipmentListScreen = new ShipmentListScreen(mAndroidDriver);
        }catch(Exception e){

        }
    }

    @Test(priority = 1)
    public void testA() throws InterruptedException{
        Assert.assertEquals(homeScreen.getToolBarText(),"Home");
        homeScreen.clickIconLogo();
        navPage.onShipmentClick();
        Assert.assertEquals(homeScreen.getToolBarText(),"Shipments");
        Thread.sleep(5000);
        shipmentListScreen.shipmentItemClick(0);

    }

    @AfterTest(alwaysRun = true)
    public void afterTest(){
        mAndroidDriver.closeApp();
        mAndroidDriver.quit();
        homeScreen = null;
        navPage = null;
    }

}
