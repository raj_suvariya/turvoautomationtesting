package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.Utilities.Constants;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by @iamBedant on 13/02/17.
 */

public class FilterScreen {

    private AndroidDriver mAndroidDriver;

    private final String scrollViewId = Constants.APP_PACKAGE_NAME+":id/pop_recycler_view";


    public FilterScreen(AndroidDriver androidDriver) {
        this.mAndroidDriver = androidDriver;
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);
    }

    @FindBy(id = Constants.APP_PACKAGE_NAME+":id/date_to")
    private WebElement toDate;

    @FindBy(id = Constants.APP_PACKAGE_NAME+":id/date_from")
    private WebElement fromeDate;

    @FindBy(className = "android.widget.ScrollView")
    private WebElement scrollView;

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\"Quote active\"))")
    private AndroidElement quoteActive;

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\"Tendered\"))")
    private AndroidElement Tendered;




    public void fromClearClicked(){
        mAndroidDriver.findElement(By.id("fromClear")).click();
    }

    public void toClearClicked(){
        mAndroidDriver.findElement(By.id("toClear")).click();
    }

    public void resetClicked(){
        mAndroidDriver.findElement(By.id("reset")).click();
    }

    public void cancelClicked(){
        mAndroidDriver.findElement(By.id("cancel")).click();
    }

    public String getToDateText(){
        return toDate.getText();
    }

    public String getFromeDateText(){
        return fromeDate.getText();
    }

    public void clickFirstPopup(){
        quoteActive.click();
    }

    public void clickSecondPopup(){
        Tendered.click();
    }

    public void clickDone(){
        mAndroidDriver.findElement(By.id("ok_dialog")).click();
    }

    public void clickPopUp(){
        mAndroidDriver.findElement(By.id("type")).click();
    }
    public void clickOnDate(){
        mAndroidDriver.findElement(By.id("date_from")).click();
    }
}
