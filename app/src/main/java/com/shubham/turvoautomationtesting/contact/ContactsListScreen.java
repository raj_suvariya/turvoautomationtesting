package com.shubham.turvoautomationtesting.contact;

import com.shubham.turvoautomationtesting.Utilities.Constants;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by Shubham on 08/02/17.
 */

public class ContactsListScreen {
    private static final String TAG = ContactsListScreen.class.getSimpleName();
    private final String scrollViewId = Constants.APP_PACKAGE_NAME+":id/list";

    private AndroidDriver mAndroidDriver;

    public ContactsListScreen(AndroidDriver androidDriver) {
        this.mAndroidDriver = androidDriver;
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);
    }

    @FindBys({@FindBy(id = Constants.APP_PACKAGE_NAME+":id/list")})
    List<WebElement> contactList;

    public String contactsItemClick(int contactIndex){
        String contactTitle= contactList.get(contactIndex).getText();
        contactList.get(contactIndex).click();
        return contactTitle;
    }

    @AndroidFindBy(uiAutomator = "UiSelector().resourceId(\""+Constants.APP_PACKAGE_NAME+":id/gridview_label\")")
    private AndroidElement toggleGridListviewButton;

    public void toggleGridListView(){
        toggleGridListviewButton.click();
    }

    @AndroidFindBy(uiAutomator = "UiSelector().resourceId(\""+Constants.APP_PACKAGE_NAME+":id/filter_label\")")
    private AndroidElement filterButton;

    public void onFilterButtonClick(){
        filterButton.click();
    }

    @AndroidFindBy(uiAutomator = "UiSelector().resourceId(\""+Constants.APP_PACKAGE_NAME+":id/sort_layout\")")
    private AndroidElement sortText;

    public void onSortTextClick(){
        sortText.click();
    }

    @AndroidFindBy(uiAutomator = "UiSelector().resourceId(\""+Constants.APP_PACKAGE_NAME+":id/filter_layout\")")
    private AndroidElement filterText;

    public void onFilterTextClick(){
        filterText.click();
    }

    @FindBys({@FindBy(id = Constants.APP_PACKAGE_NAME+":id/sort_title")})
    private List<WebElement> sortingOptionsTexts;

    public void onSortingOptionItemSelected(int position) {
        sortingOptionsTexts.get(position).click();
    }

    @FindBys({@FindBy(id = Constants.APP_PACKAGE_NAME+":id/type")})
    private List<WebElement> filterEdittexts;

    public void setKeyFilterEditText(int position, String text) {
        filterEdittexts.get(position).sendKeys(text);
    }

    public String getKeyFilterEditText(int position) {
        return filterEdittexts.get(position).getText();
    }


//    @FindBys({@FindBy(id = Constants.APP_PACKAGE_NAME+":id/filter_text")})
//    private List<WebElement> filterResultTexts;
//
//    public String getFilterResultText(int position) {
//        return filterResultTexts.get(position).getText();
//    }

    @AndroidFindBy(uiAutomator = "UiSelector().resourceId(\""+Constants.APP_PACKAGE_NAME+":id/cancel\")")
    private AndroidElement cancelButton;

    public void onCancelButtonClick(){
        cancelButton.click();
    }

    @AndroidFindBy(uiAutomator = "UiSelector().resourceId(\""+Constants.APP_PACKAGE_NAME+":id/reset\")")
    private AndroidElement resetButton;

    public void onResetButtonClick(){
        resetButton.click();
    }

    @AndroidFindBy(uiAutomator = "UiSelector().resourceId(\""+Constants.APP_PACKAGE_NAME+":id/cancel\")")
    private AndroidElement applyButton;

    public void onApplyButtonClick(){
        applyButton.click();
    }

}
