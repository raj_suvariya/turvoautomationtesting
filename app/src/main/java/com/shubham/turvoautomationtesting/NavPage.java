package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.Utilities.Constants;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by Shubham on 06/02/17.
 */

public class NavPage {

    private static final String TAG = NavPage.class.getSimpleName();
    private final String scrollViewId = Constants.APP_PACKAGE_NAME+":id/list";

    private AndroidDriver mAndroidDriver;


    public NavPage(AndroidDriver androidDriver) {
        this.mAndroidDriver = androidDriver;
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);
    }


    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\"Shipments\"))")
    private AndroidElement shipmentText;

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\""+Constants.MESSENGER+"\"))")
    private AndroidElement messengerText;


    public void onShipmentClick(){
        if(!shipmentText.isDisplayed())
            mAndroidDriver.scrollTo("Shipments");
        shipmentText.click();
    }

    public void onMessangerClick(){
        if(!messengerText.isDisplayed())
            mAndroidDriver.scrollTo(Constants.MESSENGER);
        messengerText.click();
    }

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\"Timeline\"))")
    private AndroidElement timelineText;

    public void onTimelineClick(){
        if(!timelineText.isDisplayed())
            mAndroidDriver.scrollTo("Timeline");
        timelineText.click();
    }
//    user_setttings

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().resourceId(\""+Constants.APP_PACKAGE_NAME+":id/user_setttings\"))")
    private AndroidElement accountSettingIcon;

    public void onAcSettingsIconClick(){
        accountSettingIcon.click();
    }

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\"Analytics\"))")
    private AndroidElement analyticsText;

    public void onAnalyticsClick(){
        if(!analyticsText.isDisplayed())
            mAndroidDriver.scrollTo("Analytics");
        analyticsText.click();
    }

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\"Contacts\"))")
    private AndroidElement contactText;

    public void onContactClick(){
        if(!contactText.isDisplayed())
            mAndroidDriver.scrollTo("Contacts");
        contactText.click();
    }

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\"Location\"))")
    private AndroidElement locationText;

    public void onLocationClick(){
        if(!locationText.isDisplayed())
            mAndroidDriver.scrollTo("Location");
        locationText.click();
    }

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\"Accounts\"))")
    private AndroidElement accountText;

    public void onAccountClick(){
        if(!accountText.isDisplayed())
            mAndroidDriver.scrollTo("Accounts");
        accountText.click();
    }

    @FindBys({@FindBy(id = Constants.APP_PACKAGE_NAME+":id/menu_indicator")})
    List<WebElement> navItemsMenuIndicators;

    public void onNavItemsMenuIndicatorClick(int index){
        navItemsMenuIndicators.get(index).click();
    }

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\"Customers\"))")
    private AndroidElement accountCustomerText;

    public void onAccountCustomerTextClick(){
        if(!accountCustomerText.isDisplayed())
            mAndroidDriver.scrollTo("Customers");
        accountCustomerText.click();
    }

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\"Factors\"))")
    private AndroidElement accountFactorsText;

    public void onAccountFactorsTextClick(){
        if(!accountFactorsText.isDisplayed())
            mAndroidDriver.scrollTo("Factors");
        accountFactorsText.click();
    }

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\"Carriers\"))")
    private AndroidElement accountCarriersText;

    public void onAccountCarriersTextClick(){
        if(!accountCarriersText.isDisplayed())
            mAndroidDriver.scrollTo("Carriers");
        accountCarriersText.click();
    }
}
