package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.Utilities.Constants;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by @iamBedant on 08/02/17.
 */

public class ShipmentSummeryScreen {
    private static final String TAG = ShipmentSummeryScreen.class.getSimpleName();


    private AndroidDriver mAndroidDriver;

    public ShipmentSummeryScreen(AndroidDriver androidDriver) {
        this.mAndroidDriver = androidDriver;
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);
    }


    public void onShipmentSummeryClick(){
        mAndroidDriver.findElement(By.id("filter_label")).click();
    }

}
