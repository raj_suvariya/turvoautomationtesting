package com.shubham.turvoautomationtesting.messenger;


import com.shubham.turvoautomationtesting.Utilities.Constants;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by Shubham on 12/02/17.
 */

public class MessengerScreen {

    private static final String TAG = MessengerScreen.class.getSimpleName();
    private final String scrollViewId = Constants.APP_PACKAGE_NAME+":id/list";

    private AndroidDriver mAndroidDriver;

    @AndroidFindBy(id = Constants.APP_PACKAGE_NAME+":id/fab_expand_menu_button")
    private AndroidElement favButton;

    @FindBys(@FindBy(className = "android.widget.ImageButton"))
    private List<AndroidElement> fabIconButtons;

    public MessengerScreen(AndroidDriver androidDriver) {
        this.mAndroidDriver = androidDriver;
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);
    }

    /****
     * Start a new Conversation
     */
    public void startNewConversation(){
        //Click on FAB Button
        favButton.click();
        //Click on Create Message Button -- First Fab Button is for create message
        fabIconButtons.get(0).click();
    }

    /***
     * Continue Existing Conversation
     * @param conversationName - Conversation Name
     */
    public void continueExistingConversation(String conversationName){
        String conversationView = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\""+conversationName+"\"))";

        //Wait until Conversation Box is visible
        WebDriverWait wait = new WebDriverWait(mAndroidDriver,10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(MobileBy.AndroidUIAutomator(conversationView)));
        mAndroidDriver.findElementByAndroidUIAutomator(conversationView).click();

    }

}
