package com.shubham.turvoautomationtesting.messenger;

import android.view.KeyEvent;

import com.shubham.turvoautomationtesting.Utilities.Constants;
import com.shubham.turvoautomationtesting.Utilities.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.HashMap;
import java.util.List;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by Shubham on 12/02/17.
 */

public class ConversationSettingScreen {

    private static final String TAG = ConversationSettingScreen.class.getSimpleName();
    private final String scrollViewId = Constants.APP_PACKAGE_NAME+":id/list";

    private AndroidDriver mAndroidDriver;

    public ConversationSettingScreen(AndroidDriver androidDriver) {
        this.mAndroidDriver = androidDriver;
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);
    }

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\""+Constants.SETTINGS+"\"))")
    private AndroidElement settingsTextView;

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\""+Constants.LINK+"\"))")
    private AndroidElement linkTextView;

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\""+Constants.PEOPLE+"\"))")
    private AndroidElement peopleTextView;

    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\""+Constants.ATTACHMENTS+"\"))")
    private AndroidElement attachmentsTextView;

    @FindBy(id = Constants.APP_PACKAGE_NAME+":id/back_button")
    private AndroidElement backButton;


    /****
     * Change Conversation Name
     * @param oldConversationName
     * @param newConversationName
     */
    public void changeConversationName(String oldConversationName, String newConversationName){
        WebElement conversationNameView = mAndroidDriver.findElementByAndroidUIAutomator("UiSelector().textContains(\""+oldConversationName+"\")");

        if(conversationNameView.isDisplayed()){
            conversationNameView.click();
            WebElement conversationNameEditText = mAndroidDriver.findElementById(Constants.APP_PACKAGE_NAME+":id/subject_edittext");
            if(!conversationNameEditText.getText().isEmpty())
                conversationNameEditText.clear();
            conversationNameEditText.sendKeys(newConversationName);
            Utils.pressKeyboardEnter(mAndroidDriver);
//            mAndroidDriver.sendKeyEvent(KeyEvent.KEYCODE_ENTER);
        }
    }

    /****
     *Set Mute Notification
     * @param wantToMute
     * @param index
     */
    public void setMuteNotifications(boolean wantToMute,int index) throws InterruptedException {
        settingsTextView.click();
        WebElement muteNotificationView;
        List<WebElement> textViewList;
        if(wantToMute) {
            mAndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\"" + scrollViewId + "\")).scrollIntoView(UiSelector().textContains(\"" + Constants.MUTE_NOTIFICATIONS + "\"))").click();
            Thread.sleep(2000);
            textViewList = mAndroidDriver.findElements(By.id(Constants.APP_PACKAGE_NAME+":id/item_text"));
            Thread.sleep(2000);
            textViewList.get(index).click();

        }else{
            mAndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\"" + scrollViewId + "\")).scrollIntoView(UiSelector().textContains(\"" + Constants.UNMUTE_NOTIFICATIONS + "\"))").click();
        }
    }

    public boolean getMuteNotifications(){
        settingsTextView.click();
        WebElement muteNotificationView;
        List<WebElement> textViewList;
        Boolean isMuted = false;
        try{
            mAndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\"" + scrollViewId + "\"))" +
                    ".scrollIntoView(UiSelector().textContains(\"" + Constants.UNMUTE_NOTIFICATIONS + "\"))");
            isMuted = true;
        }catch (Exception e) {

        }
        settingsTextView.click();
        return isMuted;
    }

    public void leaveGroup(boolean wantToleaveGroup){
        settingsTextView.click();
        mAndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\"" + scrollViewId + "\"))" +
                ".scrollIntoView(UiSelector().textContains(\"" + Constants.LEAVE_GROUP + "\"))").click();
        if(wantToleaveGroup) {
            mAndroidDriver.findElementByAndroidUIAutomator("UiSelector().textContains(\"" + Constants.OK + "\")").click();
        }else {
            mAndroidDriver.findElementByAndroidUIAutomator("UiSelector().textContains(\"" + Constants.CANCEL + "\")").click();
        }
    }

    public void deleteConversation(boolean wantToDeleteConversation){
        settingsTextView.click();
        mAndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\"" + scrollViewId + "\"))" +
                ".scrollIntoView(UiSelector().textContains(\"" + Constants.DELETE + "\"))").click();
        if(wantToDeleteConversation) {
            mAndroidDriver.findElementByAndroidUIAutomator("UiSelector().textContains(\"" + Constants.OK + "\")").click();
        }else {
            mAndroidDriver.findElementByAndroidUIAutomator("UiSelector().textContains(\"" + Constants.CANCEL + "\")").click();
        }
    }

    public void addPeople(){
        //Item Count View
        mAndroidDriver.findElement(By.xpath("//android.support.v7.widget.RecyclerView[0]/android.widget.RelativeLayout[3]" +
                "/android.widget.TextView[@id='"+Constants.APP_PACKAGE_NAME+":id/item_count']")).click();

        //Add People
        mAndroidDriver.findElement(By.xpath("//android.support.v7.widget.RecyclerView[0]/android.widget.RelativeLayout[3]" +
                "/android.widget.TextView[@id='"+Constants.APP_PACKAGE_NAME+":id/add_option']")).click();

    }

    public int getPeopleCount(){
        int peopleCount = 0;
        //Item Count View
        try {
            peopleCount = Integer.parseInt(mAndroidDriver.findElement(By.xpath("//android.support.v7.widget.RecyclerView[0]/android.widget.RelativeLayout[3]" +
                    "/android.widget.TextView[@id='"+Constants.APP_PACKAGE_NAME+":id/item_count']")).getText());
        }catch (Exception e){

        }
        return peopleCount;
    }


    public void addLink(){
        //Add Link
        mAndroidDriver.findElement(By.xpath("//android.support.v7.widget.RecyclerView[0]/android.widget.RelativeLayout[4]" +
                "/android.widget.TextView[@id='"+Constants.APP_PACKAGE_NAME+":id/add_option']")).click();
    }

    public int getLinkCount(){
        int linkCount = 0;
        try {
            linkCount = Integer.parseInt(mAndroidDriver.findElement(By.xpath("//android.support.v7.widget.RecyclerView[0]/android.widget.RelativeLayout[4]" +
                    "/android.widget.TextView[@id='"+Constants.APP_PACKAGE_NAME+":id/item_count']")).getText());
        }catch (Exception e){

        }
        return linkCount;
    }

    public void clickBackButton(){
        backButton.click();
    }
}

