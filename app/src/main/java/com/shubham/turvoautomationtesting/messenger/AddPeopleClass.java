package com.shubham.turvoautomationtesting.messenger;

import com.shubham.turvoautomationtesting.Utilities.Constants;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by Shubham on 15/02/17.
 */

public class AddPeopleClass {

    private static final String TAG = AddPeopleClass.class.getSimpleName();
    private final String contactListView = Constants.APP_PACKAGE_NAME+":id/contacts_list";

    private AndroidDriver mAndroidDriver;

    public AddPeopleClass(AndroidDriver androidDriver) {
        this.mAndroidDriver = androidDriver;
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);
    }

    @FindBy(id = "com.app.turvo:id/back_button")
    private AndroidElement backButton;

    @FindBy(className = "android.widget.EditText")
    AndroidElement editTextView;

    @AndroidFindBy(uiAutomator = "UiSelector().textContains(\""+Constants.DONE+"\")")
    private AndroidElement doneButton;


    public void addPeople(List<String> peopleList){
        for (String receiver : peopleList){
            editTextView.sendKeys(receiver);
            String contactView = "UiScrollable(UiSelector().resourceId(\""+contactListView+"\")).scrollIntoView(UiSelector().textContains(\""+receiver+"\"))";

            //Wait until Conversation Box is visible
            WebDriverWait wait = new WebDriverWait(mAndroidDriver,10);
            wait.until(ExpectedConditions.visibilityOfElementLocated(MobileBy.AndroidUIAutomator(contactView)));
            mAndroidDriver.findElementByAndroidUIAutomator(contactView).click();
        }
    }

    public void clickBackButton(){
        backButton.click();
    }

    public void clickDoneButton(){
        doneButton.click();
    }
}
