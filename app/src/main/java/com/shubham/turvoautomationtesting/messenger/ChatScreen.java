package com.shubham.turvoautomationtesting.messenger;

import android.view.KeyEvent;

import com.shubham.turvoautomationtesting.Utilities.Constants;
import com.shubham.turvoautomationtesting.Utilities.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by Shubham on 12/02/17.
 */

public class ChatScreen {
    private static final String TAG = ChatScreen.class.getSimpleName();
    private AndroidDriver mAndroidDriver;

    @AndroidFindBy(id = Constants.APP_PACKAGE_NAME+":id/contacts_list")
    private AndroidElement contactListView;

    @FindBy(id = Constants.APP_PACKAGE_NAME+":id/back_button")
    private AndroidElement backButton;

    //This will be used to send first message while starting new conversation
    @FindBy(id = Constants.APP_PACKAGE_NAME+":id/new_chat")
    private AndroidElement newMessageEditText;

    //This will be used to reply back message in existing conversation
    @FindBy(id = Constants.APP_PACKAGE_NAME+":id/new_message")
    private AndroidElement replyBackMessageEditText;


    public ChatScreen(AndroidDriver androidDriver) {
        this.mAndroidDriver = androidDriver;
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);
    }

    /*******
     *Add Contact Names to the Conversation
     * @param contactList - Contact's Name List
     */
    public void addContacts(List<String> contactList) throws InterruptedException {
        //Get Contact Name Edit Text View
        WebElement contactNameView = mAndroidDriver.findElementById(Constants.APP_PACKAGE_NAME+":id/searchViewLabel").findElement(By.className("android.widget.EditText"));
        //Get Contact Name String
        String receiverName = contactNameView.getText();

        //Initially Contact Name should be empty
        if(contactList!=null && receiverName.isEmpty()){
            for (String contactName : contactList){
                contactNameView.sendKeys(contactName);
                Thread.sleep(5000L);
                selectContact(contactName);
            }
        }
    }

    /***
     * Select the suggested contact to the conversation
     * Used By addContacts function
     * @param contactName - Contact Name
     */
    private void selectContact(String contactName){
        //Get Suggested Contact List Recycler View
        List<WebElement> linearLayout =  mAndroidDriver.findElementById(Constants.APP_PACKAGE_NAME+":id/contacts_list").findElements(By.className("android.widget.LinearLayout"));

        //If recycler view item text is same as contactName, then select that contact
        for (WebElement itemView : linearLayout){
            String itemViewText = itemView.findElement(By.id(Constants.APP_PACKAGE_NAME+":id/name")).getText();
            if(contactName.equalsIgnoreCase(itemViewText)){
                itemView.click();
            }
        }
    }

    /****
     * Send the First Text Message to start a conversation
     * @param messageText
     */
    public void sendFirstTextMessage(String messageText) {
        newMessageEditText.sendKeys(messageText);
        Utils.pressKeyboardEnter(mAndroidDriver);
//        mAndroidDriver.sendKeyEvent(AndroidKeyCode.ENTER);
    }

    /****
     * Reply back in an existing conversation
     * @param messageText
     */
    public void replyBackTextMessage(String messageText) {
        replyBackMessageEditText.sendKeys(messageText);
        Utils.pressKeyboardEnter(mAndroidDriver);
//        mAndroidDriver.sendKeyEvent(AndroidKeyCode.ENTER);
    }


    /***
     * Move to Settings Screen
     */
    public void moveToSetting(){
        WebElement settingView = mAndroidDriver.findElementById(Constants.APP_PACKAGE_NAME+":id/icon_next");
        settingView.click();
    }

    /****
     * Click the Back Button
     */
    public void clickBackButton(){
        backButton.click();
    }

}
