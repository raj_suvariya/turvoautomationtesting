package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.Utilities.Constants;
import com.shubham.turvoautomationtesting.Utilities.Utils;
import com.shubham.turvoautomationtesting.accountsettings.AccountSettingsScreen;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;

import io.appium.java_client.android.AndroidDriver;

/**
 * Created by kuliza-319 on 13/2/17.
 */

public class AccountSettingsTest {
    private static DesiredCapabilities dc;
    private static AndroidDriver mAndroidDriver;
    HomeScreen homeScreen;
    NavPage navPage;
    AccountSettingsScreen accountSettingsScreen;

    @BeforeSuite
    public void beforeSuite() {
        System.out.println("Before suite");
        dc= new DesiredCapabilities();
        Utils.appInit(dc);
        try {
            mAndroidDriver= new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), dc);
            homeScreen = new HomeScreen(mAndroidDriver);
            navPage = new NavPage(mAndroidDriver);
            accountSettingsScreen = new AccountSettingsScreen(mAndroidDriver);
        }catch(Exception e){
            e.printStackTrace();
            e.getMessage();
        }
    }

    @BeforeTest
    public  void beforeTest() throws InterruptedException {
        System.out.println("Before class");
        Thread.sleep(5000);
//        mAndroidDriver.resetApp();
    }

    /*
     * Test - 1
     * Unchecks one group from group list and then assert number of group selected
     * again check that group and again assert number of group selected.
     */

    @Test(priority = 1)
    public void accountSettingsGroupChangeTest() throws InterruptedException {
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.HOME);

        homeScreen.clickIconLogo();
        navPage.onAcSettingsIconClick();
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.SETTINGS);
        Thread.sleep(1000);
        accountSettingsScreen.onGroupSettingsClick();
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.GROUP);
        Thread.sleep(1000);
        accountSettingsScreen.onTestGroupToggle();
        Thread.sleep(1000);
        Assert.assertEquals(accountSettingsScreen.getNumberofCheckboxes(),2);
        accountSettingsScreen.onGrourpSettingsApplyButtonClick();
        Thread.sleep(5000);

        homeScreen.clickIconLogo();
        navPage.onAcSettingsIconClick();
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.SETTINGS);
        Thread.sleep(1000);
        accountSettingsScreen.onGroupSettingsClick();
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.GROUP);
        Thread.sleep(1000);
        accountSettingsScreen.onTestGroupToggle();
        Thread.sleep(1000);
        Assert.assertEquals(accountSettingsScreen.getNumberofCheckboxes(),3);
        accountSettingsScreen.onGrourpSettingsApplyButtonClick();
        Thread.sleep(5000);


    }

    /**
     * Test - 2
     * Toggles the theme setting and assert the state of the toggle button (ON or OFF)
     */

    @Test(priority = 2)
    public void accountSettingsThemeChangeTest() throws InterruptedException {
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.HOME);

        homeScreen.clickIconLogo();
        navPage.onAcSettingsIconClick();
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.SETTINGS);
        Thread.sleep(1000);
        accountSettingsScreen.onThemeSettingClick();
        Thread.sleep(1000);
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.THEME);
        String state = accountSettingsScreen.onThemeChangeToggleClick();
        Thread.sleep(1000);
        Assert.assertEquals(state, "ON");
        Thread.sleep(1000);
        state = accountSettingsScreen.onThemeChangeToggleClick();
        Thread.sleep(1000);
        Assert.assertEquals(state,"OFF");
        Thread.sleep(3000);
        mAndroidDriver.navigate().back();
        mAndroidDriver.navigate().back();
    }

    /**
     * Test - 3
     * Opens change password screen
     */
    @Test(priority = 3)
    public void accountSettingsChangePassTest() throws InterruptedException {
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.HOME);
        homeScreen.clickIconLogo();
        navPage.onAcSettingsIconClick();
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.SETTINGS);
        Thread.sleep(1000);
        accountSettingsScreen.onPasswordSettingClick();
        Thread.sleep(1000);
        mAndroidDriver.navigate().back();
        mAndroidDriver.navigate().back();
//        accountSettingsScreen.onBackButton();
    }

    /**
     * Test - 4
     * Logout from the app
     */
    @Test(priority = 4)
    public void accountSettingsLogoutTest() throws InterruptedException {
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.HOME);
        homeScreen.clickIconLogo();
        navPage.onAcSettingsIconClick();
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.SETTINGS);
        Thread.sleep(1000);
        accountSettingsScreen.onLogoutClick();
        Thread.sleep(1000);
        accountSettingsScreen.onConfirmLogoutButton();
        Thread.sleep(1000);
    }



    @AfterTest(alwaysRun = true)
    public void afterTest(){
        mAndroidDriver.closeApp();
        homeScreen = null;
        navPage = null;
    }

    @AfterSuite()
    public void afterSuite(){
        mAndroidDriver.quit();
    }

}
