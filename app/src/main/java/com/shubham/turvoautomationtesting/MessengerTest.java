package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.Utilities.Constants;
import com.shubham.turvoautomationtesting.Utilities.Utils;
import com.shubham.turvoautomationtesting.messenger.ChatScreen;
import com.shubham.turvoautomationtesting.messenger.ConversationSettingScreen;
import com.shubham.turvoautomationtesting.messenger.MessengerScreen;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;

/**
 * Created by Shubham on 12/02/17.
 */

public class MessengerTest {
    private static DesiredCapabilities dc;
    private static AndroidDriver mAndroidDriver;
    HomeScreen homeScreen;
    NavPage navPage;
    MessengerScreen messengerScreen;
    ChatScreen chatScreen;
    ConversationSettingScreen conversationSettingScreen;

    @BeforeSuite
    public void beforeSuite() {
        System.out.println("Before suite");
        dc= new DesiredCapabilities();
        Utils.appInit(dc);
        try {
            mAndroidDriver= new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), dc);
            homeScreen = new HomeScreen(mAndroidDriver);
            navPage = new NavPage(mAndroidDriver);
            messengerScreen = new MessengerScreen(mAndroidDriver);
            chatScreen = new ChatScreen(mAndroidDriver);
            conversationSettingScreen = new ConversationSettingScreen(mAndroidDriver);
        }catch(Exception e){

        }
    }

    @BeforeTest
    public  void beforeTest() throws InterruptedException {
        System.out.println("Before class");
//        mAndroidDriver.resetApp();
        Thread.sleep(5000);

    }


    /*****
     * Test Case 1 - Start a new conversation without any contact
     */
    @Test(priority = 1)
    public void startNewChatWithoutReceiver() throws InterruptedException {
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.HOME);
        homeScreen.clickIconLogo();
        navPage.onMessangerClick();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.MESSENGER);
        messengerScreen.startNewConversation();
        chatScreen.addContacts(null);
        chatScreen.sendFirstTextMessage("Hi");
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.NEW_MESSAGE);
        Thread.sleep(2000L);
    }

    /*****
     * Test Case 2 - Start a new conversation without any message
     */
    @Test(priority = 2)
    public void startNewChatWithoutMessage() throws InterruptedException {
        mAndroidDriver.launchApp();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.HOME);
        homeScreen.clickIconLogo();
        navPage.onMessangerClick();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.MESSENGER);
        messengerScreen.startNewConversation();

        List<String> receiverList = new ArrayList<>();
        receiverList.add("Ansu Jain");

        chatScreen.addContacts(receiverList);
        chatScreen.sendFirstTextMessage("");
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.NEW_MESSAGE);
        chatScreen.clickBackButton();
    }

    /*****
     * Test Case 3 - Start a new conversation with a contact and a message
     */
    @Test(priority = 3)
    public void startNewChat() throws InterruptedException {
        mAndroidDriver.launchApp();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.HOME);
        homeScreen.clickIconLogo();
        navPage.onMessangerClick();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.MESSENGER);
        messengerScreen.startNewConversation();

        List<String> receiverList = new ArrayList<>();
        receiverList.add("Ansu Jain");

        chatScreen.addContacts(receiverList);
        chatScreen.sendFirstTextMessage("Hello");
        Thread.sleep(10000);
        Assert.assertEquals(homeScreen.getToolBarText(), "Ansu");
    }

    /******
     *  Test Case 4 - Start a new conversation with Multiple Contacts and give that conversation a name
     *
     *  Contact List - Ansu Jain, Amandeep and Reshma Dinkar
     *  Message Text - Good Morning!!!!
     *  New Conversation Name - Turvo Chat
     */
    @Test(priority = 4)
    public void startNewChatWithConversationName() throws InterruptedException {
        mAndroidDriver.launchApp();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.HOME);
        homeScreen.clickIconLogo();
        navPage.onMessangerClick();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.MESSENGER);
        messengerScreen.startNewConversation();

        List<String> receiverList = new ArrayList<>();
        receiverList.add("Ansu Jain");
        receiverList.add("Amandeep");
        receiverList.add("Reshma Dinkar");

        chatScreen.addContacts(receiverList);
        chatScreen.sendFirstTextMessage("Good Morning!!!!");
        Thread.sleep(5000);
        Assert.assertEquals(homeScreen.getToolBarText(), "Ansu,Amandeep,Reshma");
        chatScreen.moveToSetting();
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.CONVERSATION);
        conversationSettingScreen.changeConversationName("Ansu,Amandeep,Reshma","Turvo Chat");
        conversationSettingScreen.clickBackButton();
        Assert.assertEquals(homeScreen.getToolBarText(),"Turvo Chat");

    }

    /*****
     * Test Case 5 - Continue Existing Conversation , send a message and mute the notification for one hour
     *
     * Conversation Name - Turvo Chat
     * Message - Shipment is made. Have you got it??
     */
    @Test(priority = 5)
    public void continueExistingConversationAndMuteIt() throws InterruptedException {
        mAndroidDriver.launchApp();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.HOME);
        homeScreen.clickIconLogo();
        navPage.onMessangerClick();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.MESSENGER);
        Thread.sleep(5000);
        messengerScreen.continueExistingConversation("Turvo Chat");

        chatScreen.replyBackTextMessage("Shipment is made. Have you got it??");
        Thread.sleep(5000);
        Assert.assertEquals(homeScreen.getToolBarText(), "Turvo Chat");
        chatScreen.moveToSetting();
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.CONVERSATION);
        boolean flag = conversationSettingScreen.getMuteNotifications();
        Assert.assertEquals(flag,false);
        conversationSettingScreen.setMuteNotifications(true,Constants.ONE_HOUR);
        flag = conversationSettingScreen.getMuteNotifications();
        Assert.assertEquals(flag,true);
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.CONVERSATION);
    }

    /*****
     * Test Case 6 - Continue Existing Conversation , open the leave conversation dialog and cancel it
     *
     * Conversation Name - Turvo Chat
     */
    @Test(priority = 6)
    public void continueExistingConversationOpenLeaveDialogAndCancelIt() throws InterruptedException {
        mAndroidDriver.launchApp();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.HOME);
        homeScreen.clickIconLogo();
        navPage.onMessangerClick();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.MESSENGER);
        Thread.sleep(5000);
        messengerScreen.continueExistingConversation("Turvo Chat");
        chatScreen.moveToSetting();
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.CONVERSATION);
        conversationSettingScreen.leaveGroup(false);
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.CONVERSATION);
        conversationSettingScreen.clickBackButton();
        Assert.assertEquals(homeScreen.getToolBarText(),"Turvo Chat");

    }

    /*****
     * Test Case 7 - Continue Existing Conversation , open the delete chat dialog and click Okay button
     *
     * Conversation Name - Turvo Chat
     */
    @Test(priority = 7)
    public void continueExistingConversationAndDeleteChat() throws InterruptedException {
        mAndroidDriver.launchApp();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.HOME);
        homeScreen.clickIconLogo();
        navPage.onMessangerClick();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.MESSENGER);
        Thread.sleep(5000);
        messengerScreen.continueExistingConversation("Turvo Chat");
        chatScreen.moveToSetting();
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.CONVERSATION);
        conversationSettingScreen.deleteConversation(true);
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.MESSENGER);
    }


    @AfterTest(alwaysRun = true)
    public void afterTest(){
        System.out.println("After test");
        mAndroidDriver.closeApp();
    }

    @AfterSuite()
    public void afterSuite(){
        mAndroidDriver.endTestCoverage("com.intuit.turbotaxuniversal.END_EMMA", "/data/local/coverage/coverage.ec");
        mAndroidDriver.quit();
    }
}
