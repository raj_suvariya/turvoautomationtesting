package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.Utilities.Constants;
import com.shubham.turvoautomationtesting.Utilities.Utils;
import com.shubham.turvoautomationtesting.account.AccountListScreen;

import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.List;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;

/**
 * Created by @iamBedant on 10/02/17.
 */

public class AccountTest {

    DesiredCapabilities dc;
    AndroidDriver mAndroidDriver;
    HomeScreen homeScreen;
    NavPage navPage;
    AccountListScreen accountListScreen;

    @BeforeTest
    void beforeTest(){
        dc= new DesiredCapabilities();
        Utils.appInit(dc);
        try {
            mAndroidDriver= new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), dc);
            homeScreen = new HomeScreen(mAndroidDriver);
            navPage = new NavPage(mAndroidDriver);
            accountListScreen = new AccountListScreen(mAndroidDriver);
            Thread.sleep(5000);
        }catch(Exception e){

        }
    }

    /**
     * Test - 1
     * Opens account page and toggles view between grid and list view and opens one item from each view type
     */
    @Test(priority = 1)
    public void openAccountPage() throws InterruptedException {
        Assert.assertEquals(homeScreen.getToolBarText(),"Home");
        homeScreen.clickIconLogo();
        navPage.onAccountClick();
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.ACCOUNT);
        Thread.sleep(3000);
        toogleView();
    }

    /**
     * Test - 2
     * Opens account's customer page and toggles view between grid and list view and opens one item from each view type
     */
    @Test(priority = 2)
    public void openAccountCustomerPage() throws InterruptedException {
        homeScreen.clickIconLogo();
        navPage.onAccountCustomerTextClick();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.CUSTOMER);
        Thread.sleep(3000);
        toogleView();
    }

    /**
     * Test - 3
     * Opens account's factor page and toggles view between grid and list view and opens one item from each view type
     */
    @Test(priority = 3)
    public void openAccountFactorsPage() throws InterruptedException {
        homeScreen.clickIconLogo();
        navPage.onAccountFactorsTextClick();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.FACTORS);
        Thread.sleep(3000);
        toogleView();
    }

    /**
     * Test - 4
     * Opens account's carrier page and toggles view between grid and list view and opens one item from each view type
     */
    @Test(priority = 4)
    public void openCarrierPage() throws InterruptedException {
        homeScreen.clickIconLogo();
        navPage.onAccountCarriersTextClick();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.CARRIERS);
        Thread.sleep(3000);
        toogleView();
    }

    /**
     * Changes the view type between grid and list layout and opens one item from each layout.
     */

    public void toogleView() throws InterruptedException{
        Thread.sleep(500);
        List<WebElement> accountFactorList= (new WebDriverWait(mAndroidDriver, 30)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(new MobileBy.ByAndroidUIAutomator("UiSelector().resourceId(\""+ Constants.APP_PACKAGE_NAME+":id/shipment_data_layout\")")));
        System.out.println("Size :"+accountFactorList.size());
        accountFactorList.get(0).click();
        mAndroidDriver.navigate().back();

        accountListScreen.toggleGridListView();
        for(int i=0;i<4;i++) {
            Dimension size = mAndroidDriver.manage().window().getSize();
            int y_start = (int) (size.height * 0.6);
            int y_end = (int) (size.height * 0.3);
            int x = size.width / 2;
            mAndroidDriver.swipe(x, y_start, x, y_end, 1000);
            Thread.sleep(500);
        }
        accountFactorList= (new WebDriverWait(mAndroidDriver, 30)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(new MobileBy.ByAndroidUIAutomator("UiSelector().resourceId(\""+ Constants.APP_PACKAGE_NAME+":id/entity_name\")")));
        System.out.println("Size :"+accountFactorList.size());
        accountFactorList.get(0).click();
        mAndroidDriver.navigate().back();
        Thread.sleep(1000);
        accountListScreen.toggleGridListView();
    }

    @AfterTest(alwaysRun = true)
    public void afterTest(){
        mAndroidDriver.closeApp();
        mAndroidDriver.quit();
        homeScreen = null;
        navPage = null;
    }

}
