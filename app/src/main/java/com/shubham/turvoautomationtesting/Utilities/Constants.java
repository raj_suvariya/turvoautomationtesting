package com.shubham.turvoautomationtesting.Utilities;

/**
 * Created by Shubham on 06/02/17.
 */

public class Constants {

    public static final String APP_PACKAGE_NAME = "com.app.turvo";

    public static final String HOME = "Home";
    public static final String MESSENGER = "Messenger";
    public static final String CONTACTS = "Contacts";
    public static final String TIMELINE = "Timeline";
    public static final String LOCATION = "Locations";
    public static final String CARRIERS = "Carriers";
    public static final String FACTORS = "Factors";
    public static final String CUSTOMER = "Customers";
    public static final String ACCOUNT = "Accounts";
    public static final String GROUP = "Groups";
    public static final String THEME = "Theme";

    /***
     * Messenger Testing Constants
     */
    public static final String SETTINGS = "Settings";
    public static final String LINK = "Link";
    public static final String PEOPLE = "PEOPLE";
    public static final String ATTACHMENTS = "Attachments";
    public static final String MUTE_NOTIFICATIONS = "Mute Notifications";
    public static final String UNMUTE_NOTIFICATIONS = "Unmute Notifications";
    public static final String LEAVE_GROUP = "Leave Group";
    public static final String DELETE = "Delete";
    public static final String CONVERSATION = "Conversation";

    public static final int THIRTY_MIN = 0;
    public static final int ONE_HOUR = 1;
    public static final int EIGHT_HOUR = 2;
    public static final int TWENTY_FOUR_HOUR = 3;
    public static final int UNTIL_I_TURN_BACK_ON = 4;


    public static final String NEW_MESSAGE = "New Message";
    public static final String CREATE_MESSAGE = "Create message";

    public static final String CANCEL = "CANCEL";
    public static final String OK = "OK";
    public static final String DONE = "Done";
}
