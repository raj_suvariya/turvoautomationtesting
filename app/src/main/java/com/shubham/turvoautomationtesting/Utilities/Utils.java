package com.shubham.turvoautomationtesting.Utilities;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

/**
 * Created by Shubham on 06/02/17.
 */

public class Utils {

    public static String[] months = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};


    public static void appInit(DesiredCapabilities dc){
        File classpathRoot = new File(".");
        File app = new File(classpathRoot, "app-debug.apk");
        dc.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
        dc.setCapability(MobileCapabilityType.PLATFORM_VERSION, "6.0");

//        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "PNZPMFCISSHQVSSG");
        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554 ");
        dc.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "1000");
        dc.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
        dc.setCapability("androidCoverage","com.app.turvo/com.shubham.turvoautomationtesting.Emma");
        dc.setCapability("FullReset", false);
        dc.setCapability("noReset", true);
    }

    public static void pressKeyboardEnter(AndroidDriver driver){
        Dimension size = driver.manage().window().getSize();
        int xCoordinate = size.width - 50;
        int yCoordinate = size.height - 50;
        driver.tap(1,xCoordinate,yCoordinate,125);
    }


}
