package com.shubham.turvoautomationtesting;

/**
 * Created by @iamBedant on 14/02/17.
 */

import com.shubham.turvoautomationtesting.Utilities.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DatePickerClass {
  private static final String TAG = DatePickerClass.class.getSimpleName();
  private AndroidDriver mAndroidDriver;

  public DatePickerClass(AndroidDriver mAndroidDriver) {
    this.mAndroidDriver = mAndroidDriver;
    PageFactory.initElements(new AppiumFieldDecorator(mAndroidDriver), this);
  }

  @FindBy(id = "android:id/date_picker_header_year")
  WebElement dobYearHeader;
  @FindBy(id = "android:id/date_picker_header_date")
  WebElement currentDate;          // Give initial Calender Date ie Fri,1 Feb
  @FindBy(id = "android:id/date_picker_header_year")
  WebElement currentYear;          // Give initial Calender Year ie 1980
  @FindBy(id = "android:id/prev")
  WebElement prevMonth;
  @FindBy(id = "android:id/next")
  WebElement nextMonth;
  @FindBy(id = "android:id/month_view")
  WebElement monthView;
  @FindBy(id = "android:id/button1")
  WebElement calenderOkButton;
  @FindBy(id = "android:id/button2")
  WebElement calenderCancelButton;

  public void setDate(int year,int monthOfYear,int dayOfMonth){
    String initialCalenderDate = currentDate.getText().trim();
    String initialCalenderMonth = initialCalenderDate.substring(initialCalenderDate.length()-5,initialCalenderDate.length()-2);
    int initialCalenderYear = Integer.parseInt(currentYear.getText().trim());
    System.out.println(initialCalenderMonth+" "+initialCalenderYear);

    //Set the selected year
    int interval = 4;
    if(year<initialCalenderYear){ // go backward
      while(initialCalenderYear-year>interval){
        dobYearHeader.click();
        mAndroidDriver.scrollTo(String.valueOf(initialCalenderYear-interval)).click();
        initialCalenderYear -=interval;
      }
      dobYearHeader.click();
      mAndroidDriver.scrollTo(String.valueOf(year)).click();
    }else if(year>initialCalenderYear){ // go backward
      while(year-initialCalenderYear>interval){
        dobYearHeader.click();
        mAndroidDriver.scrollTo(String.valueOf(initialCalenderYear+interval)).click();
        initialCalenderYear+=interval;
      }
      dobYearHeader.click();
      mAndroidDriver.scrollTo(String.valueOf(year)).click();
    }

    //Set the selected month
    List<String> months = new ArrayList<>(Arrays.asList(Utils.months));
    if(months.contains(initialCalenderMonth)) {
      int indexOfCurrentMonth = months.indexOf(initialCalenderMonth)+1;
      if(indexOfCurrentMonth < monthOfYear){
        while (indexOfCurrentMonth!=monthOfYear){
          nextMonth.click();
          indexOfCurrentMonth++;
        }
      }else if(indexOfCurrentMonth > monthOfYear){
        while (indexOfCurrentMonth != monthOfYear){
          prevMonth.click();
          indexOfCurrentMonth--;
        }
      }
    }

        /*
        //Set the selected year
        if(year<initialCalenderYear){
            while(initialCalenderYear-year!=0){
                int temp=12;
                while (temp!=0) {
                    prevMonth.click();
                    temp--;
                }
                initialCalenderYear--;
            }
        }else if(year>initialCalenderYear){
            while(year-initialCalenderYear!=0){
                int temp=12;
                while (temp!=0) {
                    nextMonth.click();
                    temp--;
                }
                initialCalenderYear++;
            }
        }*/
    mAndroidDriver.findElementByAndroidUIAutomator("new UiSelector().text(\""+dayOfMonth+"\")").click();
    calenderOkButton.click();

  }
}
