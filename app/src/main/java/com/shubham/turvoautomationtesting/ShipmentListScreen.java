package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.Utilities.Constants;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.HashMap;
import java.util.List;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by Shubham on 08/02/17.
 */

public class ShipmentListScreen {
    private static final String TAG = ShipmentListScreen.class.getSimpleName();
    private final String scrollViewId = Constants.APP_PACKAGE_NAME+":id/list";

    private AndroidDriver mAndroidDriver;

    public ShipmentListScreen(AndroidDriver androidDriver) {
        this.mAndroidDriver = androidDriver;
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);
    }


    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\"Shipments\"))")
    private AndroidElement shipmentText;

    @FindBys({@FindBy(id = Constants.APP_PACKAGE_NAME+":id/shipment_title")})
    List<WebElement> shipmentList;

    public String shipmentItemClick(int shipmentIndex){
        String shipmentTitle = shipmentList.get(shipmentIndex).getText();
        shipmentList.get(shipmentIndex).click();
        return shipmentTitle;
    }

}
