package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.timeline.TimelineScreen;
import com.shubham.turvoautomationtesting.Utilities.Constants;
import com.shubham.turvoautomationtesting.Utilities.Utils;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;

import io.appium.java_client.android.AndroidDriver;

/**
 * Created by kuliza-319 on 13/2/17.
 */

public class TimelineTest {
    private static DesiredCapabilities dc;
    private static AndroidDriver mAndroidDriver;
    HomeScreen homeScreen;
    NavPage navPage;
    TimelineScreen timelineScreen;

    @BeforeSuite
    public void beforeSuite() {
        System.out.println("Before suite");
        dc= new DesiredCapabilities();
        Utils.appInit(dc);
        try {
            mAndroidDriver= new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), dc);
            homeScreen = new HomeScreen(mAndroidDriver);
            navPage = new NavPage(mAndroidDriver);
            timelineScreen = new TimelineScreen(mAndroidDriver);
        }catch(Exception e){
            e.printStackTrace();
            e.getMessage();
        }
    }

    @BeforeTest
    public  void beforeTest() throws InterruptedException {
        System.out.println("Before class");
        Thread.sleep(5000);
//        mAndroidDriver.resetApp();
    }

    /**
     * Test - 1
     * Opens timeline page and scrolls down 5 times
     */
    @Test(priority = 1)
    public void timelineTest() throws InterruptedException {
        Assert.assertEquals(homeScreen.getToolBarText(),Constants.HOME);
        homeScreen.clickIconLogo();
        navPage.onTimelineClick();
        Assert.assertEquals(homeScreen.getToolBarText(), Constants.TIMELINE);
        Thread.sleep(5000);
        for(int i=0;i<5;i++) {
            Dimension size = mAndroidDriver.manage().window().getSize();
            int y_start = (int) (size.height * 0.95);
            int y_end = (int) (size.height * 0.05);
            int x = size.width / 2;
            mAndroidDriver.swipe(x, y_start, x, y_end, 1000);
            Thread.sleep(2000);
        }
    }

    @AfterTest(alwaysRun = true)
    public void afterTest(){
        mAndroidDriver.closeApp();
        homeScreen = null;
        navPage = null;
    }

    @AfterSuite()
    public void afterSuite(){
        mAndroidDriver.quit();
    }

}
