package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.Utilities.Constants;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by Shubham on 07/02/17.
 */

public class HomeScreen {
    private static final String TAG = HomeScreen.class.getSimpleName();
    private AndroidDriver mAndroidDriver;

    public HomeScreen(AndroidDriver mAndroidDriver) {
        this.mAndroidDriver = mAndroidDriver;
        PageFactory.initElements(new AppiumFieldDecorator(mAndroidDriver), this);
    }

    @FindBy(id = Constants.APP_PACKAGE_NAME+":id/icon_logo")
    private AndroidElement toolBarAppLogo;

    @FindBy(id = Constants.APP_PACKAGE_NAME+":id/toolbar_text")
    private WebElement toolBarText;

    public void clickIconLogo(){
        WebDriverWait wait = new WebDriverWait(mAndroidDriver,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Constants.APP_PACKAGE_NAME+":id/icon_logo")));
        toolBarAppLogo.click();
    }

    public String getToolBarText(){
        WebDriverWait wait = new WebDriverWait(mAndroidDriver,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Constants.APP_PACKAGE_NAME+":id/toolbar_text")));
        return toolBarText.getText();
    }


}
