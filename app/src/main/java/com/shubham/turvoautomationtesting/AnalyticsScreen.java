package com.shubham.turvoautomationtesting;

import com.shubham.turvoautomationtesting.Utilities.Constants;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by @iamBedant on 08/02/17.
 */

public class AnalyticsScreen {
    private static final String TAG = AnalyticsScreen.class.getSimpleName();
    private final String scrollViewId = Constants.APP_PACKAGE_NAME+":id/listHeaderHolder";

    private AndroidDriver mAndroidDriver;

    public AnalyticsScreen(AndroidDriver androidDriver) {
        this.mAndroidDriver = androidDriver;
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);
    }


    @AndroidFindBy(uiAutomator = "UiScrollable(UiSelector().resourceId(\""+scrollViewId+"\")).scrollIntoView(UiSelector().textContains(\"Shipment Summary\"))")
    private AndroidElement shipmentSummery;



    public void onShipmentSummeryClick(){
        if(!shipmentSummery.isDisplayed())
            mAndroidDriver.scrollTo("Shipment Summary");
        shipmentSummery.click();
    }

}
